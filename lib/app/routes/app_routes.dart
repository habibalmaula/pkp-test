part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const SPLASH = _Paths.SPLASH;
  static const ADD_POST = _Paths.ADD_POST;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const ADD_POST = '/add-post';

  static const SPLASH = '/splash';
}
