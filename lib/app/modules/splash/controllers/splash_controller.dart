import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:pkp_test/app/modules/home/bindings/home_binding.dart';
import 'package:pkp_test/app/modules/home/views/home_view.dart';

class SplashController extends GetxController {
  //TODO: Implement SplashController

  final count = 0.obs;

  @override
  void onInit() {
    Logger().i('SplashController onInit');
    Future.delayed(const Duration(seconds: 3), () {
      Get.off(() => const HomeView(), binding: HomeBinding());
    });
    super.onInit();
  }

  @override
  void onReady() {

    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void increment() => count.value++;
}
