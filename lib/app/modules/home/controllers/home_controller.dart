import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pkp_test/app/data/model/main_model.dart';
import 'package:pkp_test/app/data/networking/api_client.dart';

class HomeController extends GetxController {
  final apiClient = Get.find<ApiClient>();
  final titleController = TextEditingController();
  final bodyController = TextEditingController();

  RxList<MainModel> postsList = <MainModel>[].obs;
  Rxn<MainModel> singlePost = Rxn<MainModel>();

  final isButtonEnabled = false.obs;

  final loadState = LoadState.initial.obs;

  final remoteMesage = "".obs;

  void onInit() async {
    super.onInit();
    await getPosts(isNeedLoading: true);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  validateForm() {
    isButtonEnabled.value =
        titleController.text.isNotEmpty && bodyController.text.isNotEmpty;
  }

  Future<void> getPosts({required bool isNeedLoading}) async {
    if (isNeedLoading) {
      loadState.value = LoadState.loading;
    }
    try {
      postsList.value = await apiClient.getPosts();
      loadState.value = LoadState.loaded;
      remoteMesage.value = "Loaded";
    } catch (e) {
      loadState.value = LoadState.error;
      remoteMesage.value = e.toString();
    }
  }

  Future<void> getSinglePost({required int id}) async {
    try {
      singlePost.value = await apiClient.getSinglePost(id: id.toString());
      titleController.text = singlePost.value?.title ?? "";
      bodyController.text = singlePost.value?.body ?? "";
      loadState.value = LoadState.loaded;
      remoteMesage.value = "Loaded";
    } catch (e) {
      loadState.value = LoadState.error;
      remoteMesage.value = e.toString();
    }
  }

  Future<void> insertPost() async {
    loadState.value = LoadState.loading;
    try {
      final MainModel mainModel = MainModel(
        title: titleController.text,
        body: bodyController.text,
        userId: int.parse(ApiClient.userId),
        id: 0,
      );
      singlePost.value = await apiClient.insertPost(mainModel: mainModel);
      loadState.value = LoadState.loaded;
      remoteMesage.value = "Loaded";
    } catch (e) {
      loadState.value = LoadState.error;
      remoteMesage.value = e.toString();
    }
  }

  Future<void> updatePost() async {
    loadState.value = LoadState.loading;
    try {
      final MainModel mainModel = MainModel(
        title: titleController.text,
        body: bodyController.text,
        userId: int.parse(ApiClient.userId),
        id: int.parse(singlePost.value?.id.toString() ?? "0"),
      );
      singlePost.value = await apiClient.updatePost(mainModel: mainModel);
      loadState.value = LoadState.loaded;
      remoteMesage.value = "Loaded";
    } catch (e) {
      loadState.value = LoadState.error;
      remoteMesage.value = e.toString();
    }
  }

  Future<void> deletePost({required String id}) async {
    loadState.value = LoadState.loading;
    try {
      await apiClient.deletePost(id: id);
      loadState.value = LoadState.loaded;
      remoteMesage.value = "Loaded";
    } catch (e) {
      loadState.value = LoadState.error;
      remoteMesage.value = e.toString();
    }
  }
}

enum LoadState { initial, loading, loaded, error }
