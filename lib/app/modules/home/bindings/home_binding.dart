import 'package:get/get.dart';
import 'package:pkp_test/app/data/networking/api_client.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<ApiClient>(
          () => ApiClient(),
    );
  }
}
