import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pkp_test/app/data/constant/dimens.dart';
import 'package:pkp_test/app/data/utils.dart';
import 'package:pkp_test/app/modules/home/controllers/home_controller.dart';
import 'package:pkp_test/app/widget/bouncing_button.dart';
import 'package:pkp_test/app/widget/custom_form_field.dart';

class AddPostView extends GetView<HomeController> {
  const AddPostView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(builder: (controller) {
      return WillPopScope(
        onWillPop: () async {
          controller.titleController.text = '';
          controller.bodyController.text = '';
          controller.singlePost.value = null;
          controller.validateForm();
          return true;
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Add Post'),
            centerTitle: true,
          ),
          body: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: DimensH.mediumMargin,
                vertical: DimensV.mediumMargin),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  CustomFormField(
                      isRemovable: true,
                      onChange: (value) {
                        controller.validateForm();
                      },
                      controller: controller.titleController,
                      label: 'Title'),
                  CustomFormField(
                    isRemovable: true,
                    controller: controller.bodyController,
                    label: 'Body',
                    onChange: (value) {
                      controller.validateForm();
                    },
                    maxLines: 10,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: 8.0.w, vertical: 16.0.h),
                    child: Obx(
                      () => BouncingButton(
                        isEnabled: controller.isButtonEnabled.value,
                        isLoading:
                            controller.loadState.value == LoadState.loading,
                        bgColor: Colors.blue,
                        enableDarken: false,
                        content: Text(
                          (controller.singlePost.value != null)
                              ? 'Update'
                              : 'Add',
                          style: const TextStyle(color: Colors.white),
                        ),
                        callback: () async {
                          (controller.singlePost.value != null)
                              ? await controller.updatePost().then((value) {
                                  if (controller.loadState.value ==
                                      LoadState.loaded) {
                                    controller.titleController.text = '';
                                    controller.bodyController.text = '';
                                    controller.singlePost.value = null;
                                    controller.validateForm();
                                    Get.back();
                                    Utils.showSuccesToast(
                                        'Post updated successfully');
                                  } else {
                                    Utils.showErrorToast(
                                        controller.remoteMesage.value);
                                  }
                                })
                              : await controller.insertPost().then((value) {
                                  if (controller.loadState.value ==
                                      LoadState.loaded) {
                                    controller.titleController.text = '';
                                    controller.bodyController.text = '';
                                    Get.back();
                                    Utils.showSuccesToast(
                                        'Post added successfully');
                                  } else {
                                    Utils.showErrorToast(
                                        controller.remoteMesage.value);
                                  }
                                });
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
