import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pkp_test/app/data/utils.dart';
import 'package:pkp_test/app/modules/home/bindings/home_binding.dart';
import 'package:pkp_test/app/modules/home/views/add_post_view.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    final controller = Get.find<HomeController>();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Post List'),
        centerTitle: true,
      ),
      body: Builder(
        builder: (context) {
          return Obx(
            () => (controller.loadState.value == LoadState.loading)
                ? const Center(child: CircularProgressIndicator())
                : ((controller.loadState.value == LoadState.error)
                    ? Center(
                        child: Text(
                          controller.remoteMesage.value,
                          style: const TextStyle(fontSize: 20),
                        ),
                      )
                    : ((controller.loadState.value == LoadState.loaded &&
                            controller.postsList.isNotEmpty)
                        ? RefreshIndicator(
                            onRefresh: () async {
                              controller.getPosts(isNeedLoading: true);
                            },
                            child: ListView.builder(
                              itemCount: controller.postsList.length,
                              itemBuilder: (context, index) {
                                return Dismissible(
                                  key: UniqueKey(),
                                  background: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16.0, vertical: 8.0),
                                    decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius:
                                      BorderRadius.circular(8.0),
                                      border: Border.all(
                                        color: Colors.grey[300]!,
                                      ),
                                    ),
                                  ),
                                  onDismissed: (direction) {
                                    Utils.showSuccesToast("Post deleted");
                                    // controller.deletePost(
                                    //     controller.postsList[index].id);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0, vertical: 4.0),
                                    child: InkWell(
                                      onTap: () async {
                                        await controller
                                            .getSinglePost(
                                                id: controller
                                                    .postsList[index].id)
                                            .then((value) => Get.to(
                                                () => const AddPostView(),
                                                binding: HomeBinding()));
                                      },
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16.0, vertical: 8.0),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                          border: Border.all(
                                            color: Colors.grey[300]!,
                                          ),
                                        ),
                                        child: ListTile(
                                          title: Text(
                                            controller.postsList[index].title,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          subtitle: Text(
                                            controller.postsList[index].body,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          )
                        : Center(child: Text('No Data')))),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.to(() => const AddPostView(), binding: HomeBinding())
              ?.then((value) {
            if (value != null && value == true) {
              controller.getPosts(isNeedLoading: false);
            }
          });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
