//color theming
import 'package:flutter/material.dart';

class AppColors {
  static const Color prismWhite = Colors.white;
  static const Color prismGrey = Color(0xff282829);

  static const Color prismFillcolor = Color(0xFFFAFAFA);
  static const Color prismBordercolor = Color(0xFFEEEEEE);

  static const secondaryAccentColor = Color(0xFFE9F6FC);

  static const bordercolor = Color(0xFFEEEEEE);
  static const fillcolor = Color(0xFFFAFAFA);
  static const successColor = Color(0xFF23BF9A);
  static const successBorderColor = Color(0xFFA3D9CC);
  static const successFillColor = Color(0xFFCDEDE5);
  static const successTextColor = Color(0xFF136753);
  static const failedTextColor = Color(0xFFD91E36);
  static const failedFillColor = Color(0xFFF7D2D7);
  static const failedBgColor = Color(0xFFFFD5D3);

  static Color? prismGreen_10 = Colors.green[300];
  static Color? prismGreen_40 = Colors.green[400];
  static Color? prismGreen_50 = Colors.green[500];
  static Color? prismGreen_60 = Colors.green[600];
  static Color? prismGreen_80 = Colors.green[800];

  static Color? prismBlue_10 = Colors.blue[100];
  static Color? prismBlue_40 = Colors.blue[400];
  static Color? prismBlue_50 = Colors.blue[500];
  static Color? prismBlue_60 = Colors.blue[600];
  static Color? prismBlue_80 = Colors.blue[800];

  static Color? prismRed_10 = Colors.red[100];
  static Color? prismRed_40 = Colors.red[400];
  static Color? prismRed_50 = Colors.red[500];
  static Color? prismRed_60 = Colors.red[600];
  static Color? prismRed_80 = Colors.red[800];

  static Color? prismYellow_10 = Colors.yellow[100];
  static Color? prismYellow_40 = Colors.yellow[400];
  static Color? prismYellow_50 = Colors.yellow[500];
  static Color? prismYellow_60 = Colors.yellow[600];
  static Color? prismYellow_80 = Colors.yellow[800];

  static Color? prismOrange_10 = Colors.orange[100];
  static Color? prismOrange_40 = Colors.orange[400];
  static Color? prismOrange_50 = Colors.orange[500];
  static Color? prismOrange_60 = Colors.orange[600];
  static Color? prismOrange_80 = Colors.orange[800];

  static const Color prismBlack_05 = Colors.black12;
  static const Color prismBlack_10 = Colors.black26;
  static const Color prismBlack_40 = Colors.black38;
  static const Color prismBlack_60 = Colors.black45;
  static const Color prismBlack_80 = Colors.black54;
  static const Color prismBlack_100 = Colors.black87;
}
