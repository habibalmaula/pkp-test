import 'package:dio/dio.dart';
import 'package:pkp_test/app/data/model/main_model.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class ApiClient {
  Dio dio = Dio(
    BaseOptions(
      baseUrl: "https://jsonplaceholder.typicode.com/",
      connectTimeout: 10000,
      receiveTimeout: 10000,
      responseType: ResponseType.json,
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    ),
  )..interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));

  static const userId = "10";

  Future<List<MainModel>> getPosts() async {
    var result = await dio.get("posts?userId=$userId");
    return List<MainModel>.from(result.data.map((x) => MainModel.fromJson(x)));
  }

  Future<MainModel> getSinglePost({required String id}) async {
    var result = await dio.get("posts/$id");
    return MainModel.fromJson(result.data);
  }

  Future<MainModel> insertPost({required MainModel mainModel}) async {
    var result = await dio.post("posts", data: mainModel.toJson());
    return MainModel.fromJson(result.data);
  }

  Future<MainModel> updatePost({required MainModel mainModel}) async {
    var result =
        await dio.put("posts/${mainModel.id}", data: mainModel.toJson());
    return MainModel.fromJson(result.data);
  }

  Future<MainModel?> deletePost({required String id}) async {
    var result = await dio.delete("posts/$id");
    return MainModel.fromJson(result.data);
  }
}
